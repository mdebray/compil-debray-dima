 { bash, lib, fetchFromGitHub, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "petitc";
  version = "1.0.0";

  src = ./.;
  cargoSha256 = "sha256-I0zvc8yWGVAEtOwYTVCzVSchvpE6nmVyLKJbThIWYWk=";
  doChecks = false;
  checkInputs = [
    bash
  ];
}
