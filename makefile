petitc: src/*.rs src/*.lalrpop
	cargo build
	ln -fs target/debug/petitc petitc

.PHONY: clean release test-release

release: debray-dima.tgz

debray-dima.tgz: src/* Cargo.* makefile build.rs
	rm -rf debray-dima
	mkdir debray-dima
	cp -r src/ debray-dima
	cp -r makefile debray-dima
	cp -r Cargo.* debray-dima
	cp -r build.rs debray-dima
	cp -r README.md debray-dima
	tar -zvcf debray-dima.tgz debray-dima/
	rm -rf debray-dima

test-release: debray-dima.tgz
	tar -xvf debray-dima.tgz
	(cd debray-dima; cargo test)
	rm -rf debray-dima

clean:
	cargo clean
	rm -f petitc
