use crate::syntax::{self, BaseType, Expression, LocExpression, Statement, Type};
pub use defs::*;

mod defs;

pub fn type_file(source: &syntax::SourceFile) -> Result<Vec<TFun>, TypeError> {
    let mut env = Env::new();
    // We do not bother checking the include directives, as allowed by the petitc spec.
    env.toplevel.insert(
        "malloc",
        EnvType::FunType(
            "_custom_malloc".to_string(),
            Type(BaseType::Void, 1),
            vec![Type(BaseType::Int, 0)],
        ),
    );
    env.toplevel.insert(
        "putchar",
        EnvType::FunType(
            "_custom_putchar".to_string(),
            Type(BaseType::Int, 0),
            vec![Type(BaseType::Int, 0)],
        ),
    );
    for f in &source.declarations {
        type_function(f, &mut env, FunMangle::No)?;
    }
    match env.toplevel.get("main") {
        Some(&EnvType::FunType(_, Type(BaseType::Int, 0), ref v)) if v == &[] => Ok(env.functions),
        Some(&EnvType::FunType(_, _, _)) => Err(TypeError {
            loc: syntax::Location::Point(0),
            message: "invalid main function signature".to_string(),
        }),
        _ => Err(TypeError {
            loc: syntax::Location::Point(0),
            message: "missing main function".to_string(),
        }),
    }
}

enum FunMangle {
    Yes,
    No,
}

fn type_function<'i>(
    f: &syntax::FunctionDeclaration<'i>,
    env: &mut Env<'i>,
    mangle: FunMangle,
) -> Result<(), TypeError> {
    let label = match mangle {
        FunMangle::No => f.id.to_string(),
        FunMangle::Yes => format!("{}_{}", f.id, f.loc.start()),
    };
    let env_type = EnvType::FunType(
        label.clone(),
        f.return_type,
        Vec::from_iter(f.params.iter().map(|(t, _, _)| *t)),
    );

    env.try_insert(f.id, env_type, f.loc)?;
    env.add_fun(f.return_type);
    env.add_block();
    for (i, (t, id, loc)) in f.params.iter().enumerate() {
        // 3 is here because rbp, ra and parent rbp are stored on stack below params
        env.try_insert(
            *id,
            EnvType::VarType(*t, ((i + 3) * 8).try_into().unwrap()),
            *loc,
        )
        .map_err(|_| TypeError {
            loc: *loc,
            message: format!("parameter {id} declared twice"),
        })?;
    }

    let mut statements = type_block(&f.body, env)?;
    env.pop_block();
    // since C99, a main function without a return statement returns 0.
    if label == "main" {
        statements.push(TStatement::Return(TExpression::IntConstant(0)));
    }
    let max_offset = env.max_offset();
    env.pop_fun();
    env.functions.push(TFun(label, max_offset, statements));
    Ok(())
}

fn type_block<'i>(b: &syntax::Block<'i>, env: &mut Env<'i>) -> Result<Vec<TStatement>, TypeError> {
    let mut statements = vec![];
    for decl_or_instr in b {
        match decl_or_instr {
            syntax::DeclOrInstr::VarDecl(syntax::VarDeclaration { loc, t, id, init }) => {
                env.declare_var(*loc, id, *t)?;
                match init {
                    Some(e) => {
                        let (_, texp) = type_expression(
                            &(
                                *loc,
                                syntax::Expression::BinaryOperation(
                                    syntax::BinaryOp::Assign,
                                    Box::new((*loc, syntax::Expression::Id(id))),
                                    Box::new(e.clone()),
                                ),
                            ),
                            env,
                        )?;
                        statements.push(TStatement::Expr(texp));
                    }
                    None => (),
                }
            }
            syntax::DeclOrInstr::Instr(s) => {
                let tstat = type_statement(s, env)?;
                statements.push(tstat);
            }
            syntax::DeclOrInstr::FunDecl(f) => {
                type_function(f, env, FunMangle::Yes)?;
            }
        }
    }
    Ok(statements)
}

fn type_statement<'i>(stmt: &Statement<'i>, env: &mut Env<'i>) -> Result<TStatement, TypeError> {
    match stmt {
        Statement::Empty => Ok(TStatement::Empty),
        Statement::Expr(lexp) => Ok(TStatement::Expr(type_expression(lexp, env)?.1)),
        Statement::Return { loc, val: None } => {
            if env.ret_type() == Type(BaseType::Void, 0) {
                Ok(TStatement::ReturnNothing)
            } else {
                Err(TypeError {
                    loc: *loc,
                    message: format!("expected a return value of type {}", env.ret_type()),
                })
            }
        }
        Statement::Return {
            loc,
            val: Some(lexp),
        } => {
            let (t, texp) = type_expression(lexp, env)?;
            Ok(TStatement::Return(
                get_conversion(t, env.ret_type(), *loc)?.convert(texp),
            ))
        }
        Statement::IfElse {
            condition,
            s_if,
            s_else,
            tag,
        } => {
            let (t, texp) = type_expression(condition, env)?;
            if t == Type(BaseType::Void, 0) {
                Err(TypeError {
                    loc: condition.0,
                    message: "stop trying to use values of void type, not gonna happen".to_string(),
                })
            } else {
                let tif = type_statement(s_if, env)?;
                let telse = type_statement(s_else, env)?;
                Ok(TStatement::IfElse(
                    texp,
                    Box::new(tif),
                    Box::new(telse),
                    *tag,
                ))
            }
        }
        Statement::Break(loc) => match env.loop_tag() {
            Some(l) => Ok(TStatement::Break(l)),
            None => Err(TypeError {
                loc: *loc,
                message: "break statement not within loop".to_string(),
            }),
        },
        Statement::Continue(loc) => match env.loop_tag() {
            Some(l) => Ok(TStatement::Continue(l)),
            None => Err(TypeError {
                loc: *loc,
                message: "continue statement not within loop".to_string(),
            }),
        },
        Statement::For {
            condition,
            after,
            body,
            tag,
        } => {
            let (t, texp) = type_expression(condition, env)?;
            if t == Type(BaseType::Void, 0) {
                Err(TypeError {
                    loc: condition.0,
                    message: "loop condition cannot have void type".to_string(),
                })
            } else {
                env.add_loop_block(*tag);
                let tbody = type_statement(body, env)?;
                env.pop_block();
                Ok(TStatement::For(
                    texp,
                    after
                        .iter()
                        .map(|le| Ok(type_expression(le, env)?.1))
                        .collect::<Result<Vec<_>, _>>()?,
                    Box::new(tbody),
                    *tag,
                ))
            }
        }
        Statement::Block(b) => {
            env.add_block();
            let statements = type_block(b, env)?;
            env.pop_block();
            Ok(TStatement::Block(statements))
        }
    }
}

fn type_expression<'i>(
    exp: &LocExpression<'i>,
    env: &Env,
) -> Result<(Type, TExpression), TypeError> {
    match exp {
        (_, Expression::IntConstant(i)) => {
            Ok((Type(BaseType::Int, 0), TExpression::IntConstant(*i)))
        }
        (_, Expression::CharConstant(c)) => Ok((
            Type(BaseType::Int, 0),
            TExpression::IntConstant(u32::from(*c) as i64),
        )),
        (_, Expression::BoolConstant(b)) => Ok((
            Type(BaseType::Bool, 0),
            TExpression::IntConstant(i64::from(*b)),
        )),
        (_, Expression::NullConstant) => Ok((Type(BaseType::Void, 1), TExpression::IntConstant(0))),
        (loc, Expression::Id(id)) => match env.lookup(id) {
            None => Err(TypeError {
                loc: *loc,
                message: format!("identifier {id} undeclared"),
            }),
            Some((EnvType::VarType(t, offset), depth)) => Ok((
                *t,
                TExpression::UnaryOperation(
                    TUnaryOp::Deref,
                    Box::new(TExpression::VarAddress(*offset, depth)),
                ),
            )),
            Some((EnvType::FunType(_, _, _), _)) => Err(TypeError {
                loc: *loc,
                message: format!("identifier {id} refers to a function, not a variable"),
            }),
        },
        (loc, Expression::FuncCall(id, args)) => match env.lookup(id) {
            None => Err(TypeError {
                loc: *loc,
                message: format!("identifier {id} undeclared"),
            })?,
            Some((EnvType::VarType(t, _), _)) => Err(TypeError {
                loc: *loc,
                message: format!(
                    "identifier {id} refers to a variable with type {t}, not a function"
                ),
            }),
            Some((EnvType::FunType(label, ret_type, argtypes), depth)) => {
                if argtypes.len() == args.len() {
                    let targs: Result<Vec<_>, _> = args
                        .iter()
                        .zip(argtypes)
                        .map(|(arg, &argtype)| {
                            let (t, texp) = type_expression(arg, env)?;
                            Ok(get_conversion(t, argtype, arg.0)?.convert(texp))
                        })
                        .collect();
                    Ok((
                        *ret_type,
                        TExpression::FuncCall(label.clone(), targs?, depth),
                    ))
                } else {
                    Err(TypeError {
                        loc: *loc,
                        message: format!(
                            "incorrect number of arguments to function {id}: expected {}, got {}",
                            argtypes.len(),
                            args.len()
                        ),
                    })
                }
            }
        },
        (loc, Expression::UnaryOperation(op, operand)) => type_unop(*loc, *op, operand, env),
        (loc, Expression::BinaryOperation(op, lexp, rexp)) => {
            type_binop(*loc, *op, lexp, rexp, env)
        }
        (loc, Expression::SizeOf(Type(BaseType::Void, 0))) => Err(TypeError {
            loc: *loc,
            message: "void type has no size".to_string(),
        }),
        (_, Expression::SizeOf(_)) => {
            // Every type is stored in 64 bits
            Ok((Type(syntax::BaseType::Int, 0), TExpression::IntConstant(8)))
        }
    }
}

fn type_unop<'i>(
    loc: syntax::Location,
    op: syntax::UnaryOp,
    operand: &LocExpression<'i>,
    env: &Env,
) -> Result<(Type, TExpression), TypeError> {
    let (t, texp) = type_expression(operand, env)?;
    match op {
        syntax::UnaryOp::Plus => Ok((
            Type(BaseType::Int, 0),
            get_conversion(t, Type(BaseType::Int, 0), loc)?.convert(texp),
        )),
        syntax::UnaryOp::Minus => Ok((
            Type(BaseType::Int, 0),
            TExpression::UnaryOperation(
                TUnaryOp::Minus,
                Box::new(get_conversion(t, Type(BaseType::Int, 0), loc)?.convert(texp)),
            ),
        )),
        syntax::UnaryOp::Not => match t {
            Type(BaseType::Void, 0) => Err(TypeError {
                loc,
                message: "argument of unary `!` has type void".to_string(),
            }),
            _ => Ok((
                Type(BaseType::Int, 0),
                TExpression::UnaryOperation(TUnaryOp::Not, Box::new(texp)),
            )),
        },
        syntax::UnaryOp::Reference => Ok((Type(t.0, t.1 + 1), as_lvalue(texp, loc)?)),
        syntax::UnaryOp::Deref => match t {
            Type(_, 0) => Err(TypeError {
                loc,
                message: format!("value to be dereferenced must have pointer type, has type {t}"),
            }),
            Type(BaseType::Void, 1) => Err(TypeError {
                loc,
                message: "can't dereference void pointer".to_string(),
            }),
            Type(base, i) => Ok((
                Type(base, i - 1),
                TExpression::UnaryOperation(TUnaryOp::Deref, Box::new(texp)),
            )),
        },
        syntax::UnaryOp::PreIncrement
        | syntax::UnaryOp::PostIncrement
        | syntax::UnaryOp::PreDecrement
        | syntax::UnaryOp::PostDecrement => {
            let (crement_time, crement_op) = match op {
                syntax::UnaryOp::PreIncrement => (CrementTime::Pre, CrementOp::Increment),
                syntax::UnaryOp::PostIncrement => (CrementTime::Post, CrementOp::Increment),
                syntax::UnaryOp::PreDecrement => (CrementTime::Pre, CrementOp::Decrement),
                syntax::UnaryOp::PostDecrement => (CrementTime::Post, CrementOp::Decrement),
                _ => unreachable!(),
            };
            let crement_type = match t {
                Type(BaseType::Int, 0) => CrementType::Int,
                Type(BaseType::Bool, 0) => CrementType::Bool,
                Type(BaseType::Void, 0) => {
                    return Err(TypeError {
                        loc,
                        message: "cannot use void value".to_string(),
                    })
                }
                Type(_, _) => CrementType::Pointer,
            };
            Ok((
                t,
                TExpression::UnaryOperation(
                    TUnaryOp::Crement(crement_op, crement_time, crement_type),
                    Box::new(as_lvalue(texp, loc)?),
                ),
            ))
        }
    }
}

fn type_binop<'i>(
    loc: syntax::Location,
    op: syntax::BinaryOp,
    lexp: &LocExpression<'i>,
    rexp: &LocExpression<'i>,
    env: &Env,
) -> Result<(Type, TExpression), TypeError> {
    let (ltype, left) = type_expression(lexp, env)?;
    let (rtype, right) = type_expression(rexp, env)?;
    match op {
        syntax::BinaryOp::Assign => Ok((
            ltype,
            TExpression::BinaryOperation(
                TBinaryOp::Assign,
                Box::new(as_lvalue(left, loc)?),
                Box::new(get_conversion(ltype, rtype, loc)?.convert(right)),
            ),
        )),
        syntax::BinaryOp::Equal
        | syntax::BinaryOp::NotEqual
        | syntax::BinaryOp::Less
        | syntax::BinaryOp::Leq => {
            if ltype == Type(BaseType::Void, 0) {
                Err(TypeError {
                    loc,
                    message: "void value may not be used".to_string(),
                })
            } else {
                // We do not use the conversion since in any case a nop is fine
                get_conversion(rtype, ltype, loc)?;
                Ok((
                    Type(BaseType::Int, 0),
                    TExpression::BinaryOperation(
                        match op {
                            syntax::BinaryOp::Equal => TBinaryOp::Equal,
                            syntax::BinaryOp::NotEqual => TBinaryOp::NotEqual,
                            syntax::BinaryOp::Less => TBinaryOp::Less,
                            syntax::BinaryOp::Leq => TBinaryOp::Leq,
                            _ => unreachable!(),
                        },
                        Box::new(left),
                        Box::new(right),
                    ),
                ))
            }
        }
        syntax::BinaryOp::Greater => type_binop(loc, syntax::BinaryOp::Less, rexp, lexp, env),
        syntax::BinaryOp::Geq => type_binop(loc, syntax::BinaryOp::Leq, rexp, lexp, env),
        syntax::BinaryOp::Add => {
            match (
                get_conversion(ltype, Type(BaseType::Int, 0), loc),
                get_conversion(rtype, Type(BaseType::Int, 0), loc),
            ) {
                (Ok(lc), Ok(rc)) => Ok((
                    Type(BaseType::Int, 0),
                    TExpression::BinaryOperation(
                        TBinaryOp::Add,
                        Box::new(lc.convert(left)),
                        Box::new(rc.convert(right)),
                    ),
                )),
                (Ok(c), Err(_)) => {
                    if rtype.1 > 0 {
                        Ok((
                            rtype,
                            TExpression::BinaryOperation(
                                TBinaryOp::Add,
                                Box::new(TExpression::Pointerfy(Box::new(c.convert(left)))),
                                Box::new(right),
                            ),
                        ))
                    } else {
                        Err(TypeError {
                            loc,
                            message: format!("cannot add types {ltype} and {rtype}"),
                        })
                    }
                }
                (Err(_), Ok(c)) => {
                    if ltype.1 > 0 {
                        Ok((
                            ltype,
                            TExpression::BinaryOperation(
                                TBinaryOp::Add,
                                Box::new(TExpression::Pointerfy(Box::new(c.convert(right)))),
                                Box::new(left),
                            ),
                        ))
                    } else {
                        Err(TypeError {
                            loc,
                            message: format!("cannot add types {ltype} and {rtype}"),
                        })
                    }
                }
                (Err(_), Err(_)) => Err(TypeError {
                    loc,
                    message: format!("cannot add types {ltype} and {rtype}"),
                }),
            }
        }
        syntax::BinaryOp::Sub => {
            match (
                get_conversion(ltype, Type(BaseType::Int, 0), loc),
                get_conversion(rtype, Type(BaseType::Int, 0), loc),
            ) {
                (Ok(lc), Ok(rc)) => Ok((
                    Type(BaseType::Int, 0),
                    TExpression::BinaryOperation(
                        TBinaryOp::Sub,
                        Box::new(lc.convert(left)),
                        Box::new(rc.convert(right)),
                    ),
                )),
                (Err(_), Ok(c)) => {
                    if ltype.1 > 0 {
                        Ok((
                            ltype,
                            TExpression::BinaryOperation(
                                TBinaryOp::Sub,
                                Box::new(left),
                                Box::new(TExpression::Pointerfy(Box::new(c.convert(right)))),
                            ),
                        ))
                    } else {
                        Err(TypeError {
                            loc,
                            message: format!("cannot subtract type {rtype} from {ltype}"),
                        })
                    }
                }
                // Ugly match guard for pointer subtraction
                (Err(_), Err(_))
                    if match (ltype, rtype) {
                        (Type(BaseType::Void, 1), Type(_, i)) if i > 0 => true,
                        (Type(_, i), Type(BaseType::Void, 1)) if i > 0 => true,
                        (Type(base_l, i), Type(base_r, j))
                            if base_l == base_r && i == j && i > 0 =>
                        {
                            true
                        }
                        _ => false,
                    } =>
                {
                    Ok((
                        Type(BaseType::Int, 0),
                        TExpression::DePointerfy(Box::new(TExpression::BinaryOperation(
                            TBinaryOp::Sub,
                            Box::new(left),
                            Box::new(right),
                        ))),
                    ))
                }
                _ => Err(TypeError {
                    loc,
                    message: format!("cannot add types {ltype} and {rtype}"),
                }),
            }
        }
        syntax::BinaryOp::Mul | syntax::BinaryOp::Div | syntax::BinaryOp::Mod => match (
            get_conversion(ltype, Type(BaseType::Int, 0), loc),
            get_conversion(rtype, Type(BaseType::Int, 0), loc),
        ) {
            (Ok(lc), Ok(rc)) => Ok((
                Type(BaseType::Int, 0),
                TExpression::BinaryOperation(
                    match op {
                        syntax::BinaryOp::Mul => TBinaryOp::Mul,
                        syntax::BinaryOp::Div => TBinaryOp::Div,
                        syntax::BinaryOp::Mod => TBinaryOp::Mod,
                        _ => unreachable!(),
                    },
                    Box::new(lc.convert(left)),
                    Box::new(rc.convert(right)),
                ),
            )),
            _ => Err(TypeError {
                loc,
                message: format!("cannot apply binary operation {op} to types {ltype} and {rtype}"),
            }),
        },

        syntax::BinaryOp::And => match (
            get_conversion(ltype, Type(BaseType::Bool, 0), loc),
            get_conversion(rtype, Type(BaseType::Bool, 0), loc),
        ) {
            (Ok(lc), Ok(rc)) => Ok((
                Type(BaseType::Bool, 0),
                TExpression::LazyAnd(
                    Box::new(lc.convert(left)),
                    Box::new(rc.convert(right)),
                    loc.start(),
                ),
            )),
            _ => Err(TypeError {
                loc,
                message: format!("cannot apply binary and to types {ltype} and {rtype}"),
            }),
        },
        syntax::BinaryOp::Or => match (
            get_conversion(ltype, Type(BaseType::Bool, 0), loc),
            get_conversion(rtype, Type(BaseType::Bool, 0), loc),
        ) {
            (Ok(lc), Ok(rc)) => Ok((
                Type(BaseType::Bool, 0),
                TExpression::LazyOr(
                    Box::new(lc.convert(left)),
                    Box::new(rc.convert(right)),
                    loc.start(),
                ),
            )),
            _ => Err(TypeError {
                loc,
                message: format!("cannot apply binary or to types {ltype} and {rtype}"),
            }),
        },
    }
}

enum ConversionType {
    Nop,
    IntToBool,
}

impl ConversionType {
    fn convert(&self, texp: TExpression) -> TExpression {
        match self {
            Self::Nop => texp,
            Self::IntToBool => TExpression::IntToBoolCast(Box::new(texp)),
        }
    }
}

/// Express automatic casts of bool to int and void* to whatever*
/// Return an error if the type doesn't match
fn get_conversion(
    starttype: Type,
    desttype: Type,
    loc: syntax::Location,
) -> Result<ConversionType, TypeError> {
    match (starttype, desttype) {
        (Type(BaseType::Int, 0), Type(BaseType::Bool, 0)) => Ok(ConversionType::IntToBool),
        (Type(BaseType::Bool, 0), Type(BaseType::Int, 0)) => Ok(ConversionType::Nop),
        (Type(_, i), Type(BaseType::Void, 1)) | (Type(BaseType::Void, 1), Type(_, i)) if i >= 1 => {
            Ok(ConversionType::Nop)
        }
        (i, j) if i == j => Ok(ConversionType::Nop),
        _ => Err(TypeError {
            loc,
            message: format!("type {starttype} cannot be converted to expected type {desttype}"),
        }),
    }
}

fn as_lvalue(e: TExpression, loc: syntax::Location) -> Result<TExpression, TypeError> {
    match e {
        TExpression::UnaryOperation(TUnaryOp::Deref, b) => Ok(*b),
        TExpression::VarAddress(offset, depth) => Ok(TExpression::VarAddress(offset, depth)),
        _ => Err(TypeError {
            loc,
            message: "expected lvalue".to_string(),
        }),
    }
}
