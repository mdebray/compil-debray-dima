use crate::syntax::{BaseType, Identifier, Location, Type};
use crate::CompilerError;
use std::collections::HashMap;

pub type StatementTag = usize;
pub type FunLabel = String;
pub type Offset = i64;

#[derive(Debug, Eq, PartialEq, thiserror::Error)]
#[error("type error: {message}")]
pub struct TypeError {
    pub loc: Location,
    pub message: String,
}

impl CompilerError for TypeError {
    fn message(&self) -> String {
        format!("{self}")
    }

    fn location(&self) -> Location {
        self.loc
    }
}

#[derive(Debug)]
pub enum TExpression {
    IntConstant(i64),
    UnaryOperation(TUnaryOp, Box<TExpression>),
    BinaryOperation(TBinaryOp, Box<TExpression>, Box<TExpression>),
    VarAddress(Offset, usize),
    IntToBoolCast(Box<TExpression>),
    // The usize is the number of "static" (as called in jcf's paper) parent stack frame
    // indirections to go through (yeah this is very badly explained)
    FuncCall(FunLabel, Vec<TExpression>, usize),
    // Bit shifts to turn pointer offsets into address offsets
    Pointerfy(Box<TExpression>),
    DePointerfy(Box<TExpression>),
    LazyOr(Box<TExpression>, Box<TExpression>, StatementTag),
    LazyAnd(Box<TExpression>, Box<TExpression>, StatementTag),
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum TBinaryOp {
    Assign,
    Equal,
    NotEqual,
    Less,
    Leq,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
}

#[derive(Debug)]
pub enum TUnaryOp {
    Minus,
    Not,
    Deref,
    Crement(CrementOp, CrementTime, CrementType),
}

#[derive(Debug)]
pub enum CrementOp {
    Increment,
    Decrement,
}

#[derive(Debug)]
pub enum CrementTime {
    Pre,
    Post,
}

#[derive(Debug)]
pub enum CrementType {
    Int,
    Bool,
    Pointer,
}

#[derive(Debug)]
pub enum TStatement {
    Empty,
    Expr(TExpression),
    ReturnNothing,
    Return(TExpression),
    IfElse(TExpression, Box<TStatement>, Box<TStatement>, StatementTag),
    Break(StatementTag),
    Continue(StatementTag),
    For(TExpression, Vec<TExpression>, Box<TStatement>, StatementTag),
    Block(Vec<TStatement>),
}

#[derive(Debug)]
pub struct TFun(
    pub FunLabel,
    /// Space to allocate for local variables
    pub Offset,
    pub Vec<TStatement>,
);

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum EnvType {
    VarType(
        /// Type of this variable
        Type,
        /// Offset at which to find this variable
        Offset,
    ),
    FunType(
        /// Label
        FunLabel,
        /// Return type
        Type,
        /// Argument types
        Vec<Type>,
    ),
}
struct FunLevel<'i> {
    ret_type: Type,
    blocks: Vec<BlockLevel<'i>>,
    max_offset: Offset,
}

struct BlockLevel<'i> {
    loop_tag: Option<StatementTag>,
    names: HashMap<Identifier<'i>, EnvType>,
    current_offset: Offset,
}
impl<'i> BlockLevel<'i> {
    fn new() -> Self {
        Self {
            loop_tag: None,
            names: HashMap::new(),
            current_offset: 0,
        }
    }
}

pub struct Env<'i> {
    pub toplevel: HashMap<Identifier<'i>, EnvType>,
    funlevels: Vec<FunLevel<'i>>,
    pub functions: Vec<TFun>,
}
impl<'i> Env<'i> {
    pub fn new() -> Self {
        Self {
            funlevels: Vec::new(),
            toplevel: HashMap::new(),
            functions: Vec::new(),
        }
    }
    /// The returned usize determines the number of function scopes we have to go up through to
    /// find the declaration.
    /// int f() {
    ///     int a;
    ///     void g() {
    ///         int c;
    ///     }
    /// }
    /// For example, from inside the body of g, finding c goes through 0 scopes, finding a and g
    /// goes through 1 and finding f goes through 2.
    pub fn lookup(&self, id: Identifier<'i>) -> Option<(&EnvType, usize)> {
        for (i, funlevel) in self.funlevels.iter().rev().enumerate() {
            for BlockLevel {
                loop_tag: _,
                current_offset: _,
                names,
            } in funlevel.blocks.iter().rev()
            {
                if let Some(t) = names.get(id) {
                    return Some((t, i));
                }
            }
        }
        self.toplevel.get(id).map(|x| (x, self.funlevels.len()))
    }

    fn insert(&mut self, id: Identifier<'i>, t: EnvType) -> Option<EnvType> {
        self.funlevels
            .last_mut()
            .unwrap()
            .blocks
            .last_mut()
            .unwrap()
            .names
            .insert(id, t)
    }

    pub fn try_insert(
        &mut self,
        id: Identifier<'i>,
        t: EnvType,
        loc: Location,
    ) -> Result<(), TypeError> {
        if self.funlevels.is_empty() {
            if self.toplevel.insert(id, t).is_some() {
                Err(TypeError {
                    loc,
                    message: format!("function {id} already defined"),
                })
            } else {
                Ok(())
            }
        } else if self.insert(id, t).is_some() {
            Err(TypeError {
                loc,
                message: format!("function or variable {id} already present"),
            })
        } else {
            Ok(())
        }
    }

    pub fn declare_var(
        &mut self,
        loc: Location,
        id: Identifier<'i>,
        t: Type,
    ) -> Result<(), TypeError> {
        if let Type(BaseType::Void, 0) = t {
            return Err(TypeError {
                loc,
                message: "variable may not have void type".to_string(),
            });
        }

        let max_offset = self.max_offset();
        let next_offset = self.current_offset() - 8;
        self.top_block_mut().current_offset = next_offset;
        if next_offset < max_offset {
            self.top_fun_mut().max_offset = next_offset;
        }
        self.try_insert(id, EnvType::VarType(t, next_offset), loc)
    }

    pub fn add_fun(&mut self, ret_type: Type) {
        self.funlevels.push(FunLevel {
            ret_type,
            blocks: vec![],
            max_offset: 0,
        });
    }

    pub fn add_block(&mut self) {
        if self.top_fun().blocks.is_empty() {
            self.top_fun_mut().blocks.push(BlockLevel::new());
        } else {
            let loop_tag = self.loop_tag();
            let current_offset = self.current_offset();
            self.funlevels.last_mut().unwrap().blocks.push(BlockLevel {
                loop_tag,
                current_offset,
                names: HashMap::new(),
            });
        }
    }

    pub fn add_loop_block(&mut self, tag: StatementTag) {
        self.add_block();
        self.top_block_mut().loop_tag = Some(tag);
    }

    pub fn pop_fun(&mut self) {
        self.funlevels.pop();
    }

    pub fn pop_block(&mut self) {
        self.top_fun_mut().blocks.pop();
    }

    pub fn ret_type(&self) -> Type {
        self.top_fun().ret_type
    }

    pub fn loop_tag(&self) -> Option<StatementTag> {
        self.top_block().loop_tag
    }

    fn current_offset(&self) -> Offset {
        self.top_block().current_offset
    }

    pub fn max_offset(&self) -> Offset {
        self.top_fun().max_offset
    }

    fn top_fun(&self) -> &FunLevel {
        self.funlevels.last().unwrap()
    }

    fn top_fun_mut(&mut self) -> &mut FunLevel<'i> {
        self.funlevels.last_mut().unwrap()
    }

    fn top_block(&self) -> &BlockLevel {
        self.top_fun().blocks.last().unwrap()
    }

    fn top_block_mut(&mut self) -> &mut BlockLevel<'i> {
        self.top_fun_mut().blocks.last_mut().unwrap()
    }
}
