use crate::syntax::{Location, SyntaxError};
use std::fmt::Write;
use std::iter::Peekable;
use std::num::IntErrorKind;
use std::str::CharIndices;
use std::str::FromStr;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Token<'i> {
    Include(&'i str),
    CharLiteral(char),
    IntLiteral(i64),
    Identifier(&'i str),

    // Keywords.
    Bool,
    Break,
    Continue,
    Else,
    False,
    For,
    If,
    Int,
    Null,
    Return,
    SizeOf,
    True,
    Void,
    While,

    // Structuring symbols.
    LeftPar,
    RightPar,
    LeftBracket,
    RightBracket,
    LeftBrace,
    RightBrace,
    Semicolon,
    Comma,

    // Operators.
    Assign,
    Or,
    And,
    Equal,
    NotEqual,
    Less,
    Leq,
    Greater,
    Geq,
    Plus,
    Minus,
    Star,
    Slash,
    Percent,
    Exclamation,
    Ampersand,
    Increment,
    Decrement,
}

impl<'i> std::fmt::Display for Token<'i> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Shenanigans.
        // Least ugly way found to make the borrow checker happy
        let mut survive = String::new();
        write!(
            f,
            "{}",
            match self {
                Token::Include(incl) => {
                    write!(survive, "#include<{incl}>\n").unwrap();
                    survive.as_str()
                }
                Token::CharLiteral(c) => {
                    write!(survive, "{c:?}").unwrap();
                    survive.as_str()
                }
                Token::IntLiteral(n) => {
                    write!(survive, "{n}").unwrap();
                    survive.as_str()
                }
                Token::Identifier(id) => {
                    write!(survive, "{id}").unwrap();
                    survive.as_str()
                }
                Token::Bool => "bool",
                Token::Break => "break",
                Token::Continue => "continue",
                Token::Else => "else",
                Token::False => "false",
                Token::For => "for",
                Token::If => "if",
                Token::Int => "int",
                Token::Null => "NULL",
                Token::Return => "return",
                Token::SizeOf => "sizeof",
                Token::True => "true",
                Token::Void => "void",
                Token::While => "while",
                Token::LeftPar => "(",
                Token::RightPar => ")",
                Token::LeftBracket => "[",
                Token::RightBracket => "]",
                Token::LeftBrace => "{",
                Token::RightBrace => "}",
                Token::Semicolon => ";",
                Token::Comma => ",",
                Token::Assign => "=",
                Token::Or => "||",
                Token::And => "&&",
                Token::Equal => "==",
                Token::NotEqual => "!=",
                Token::Less => "<",
                Token::Leq => "<=",
                Token::Greater => ">",
                Token::Geq => ">=",
                Token::Plus => "+",
                Token::Minus => "-",
                Token::Star => "*",
                Token::Slash => "/",
                Token::Percent => "%",
                Token::Exclamation => "!",
                Token::Ampersand => "&",
                Token::Increment => "++",
                Token::Decrement => "--",
            }
        )
    }
}

pub struct Lexer<'i> {
    chars: Peekable<CharIndices<'i>>,
    input: &'i str,
}

impl<'i> Lexer<'i> {
    pub fn new(input: &'i str) -> Self {
        Lexer {
            chars: input.char_indices().peekable(),
            input,
        }
    }
}

impl<'i> Iterator for Lexer<'i> {
    type Item = Result<(usize, Token<'i>, usize), SyntaxError>;

    fn next(&mut self) -> Option<Self::Item> {
        // Use a loop here so we can reenter without recursing after encountering whitespace or
        // a comment.
        'outer: loop {
            match self.chars.next() {
                // Whitespace is ignored.
                // This includes ' ', '\t' and '\n', but also the Carriage Return and Form Feed
                // characters; see the rust doc on char::is_ascii_whitespace.
                // The specification only explicitly mentions space, tab and line feed as
                // whitespace, but seeing as gcc accepts other whitespace characters than those
                // three, we consider it unlikely that any evaluation tests will test this aspect.
                // And yes, the time spent writing this comment might have been better spent
                // improving the error messages.
                Some((_, s)) if s.is_ascii_whitespace() => {
                    continue 'outer;
                }

                Some((i, '/')) => match self.chars.peek() {
                    // Line comment.
                    Some((_, '/')) => {
                        for (_, c) in self.chars.by_ref() {
                            if c == '\n' {
                                break;
                            }
                        }
                        // At loop exit, we have either consumed a newline or reached EOF.
                        continue 'outer;
                    }

                    // Block comment.
                    Some((_, '*')) => {
                        self.chars.next();
                        while let Some((_, c)) = self.chars.next() {
                            if c == '*' {
                                if let Some((_, '/')) = self.chars.peek() {
                                    // End of a block comment.
                                    self.chars.next();
                                    continue 'outer;
                                }
                            }
                        }
                        // EOF
                        return Some(Err(SyntaxError {
                            loc: Location::Range(i, self.input.len()),
                            message: "unterminated block comment".to_string(),
                        }));
                    }

                    // Neither a block comment nor a line comment; just a '/'.
                    _ => {
                        return Some(Ok((i, Token::Slash, i + 1)));
                    }
                },

                // #include directive.
                Some((i, '#')) => {
                    for c in "include".chars() {
                        {
                            match self.chars.next() {
                                Some((_, d)) if d == c => {}
                                Some((j, d)) => {
                                    return Some(Err(SyntaxError {
                                        loc: Location::Point(j),
                                        message: format!("invalid character in include keyword: expected {c:?}, found {d:?}"),
                                    }));
                                }
                                None => {
                                    return Some(Err(SyntaxError {
                                        loc: Location::Point(self.input.len()),
                                        message: format!(
                                            "unexpected EOF in include keyword: expected {c:?}"
                                        ),
                                    }));
                                }
                            }
                        }
                    }

                    while let Some((_, ' ')) = self.chars.peek() {
                        self.chars.next();
                    }

                    let j = 1 + match self.chars.next() {
                        Some((j, '<')) => j,
                        Some((j, c)) => {
                            return Some(Err(SyntaxError {
                                loc: Location::Point(j),
                                message: format!(
                                "invalid character in include directive: expected '<', found {c:?}"
                            ),
                            }))
                        }

                        None => {
                            return Some(Err(SyntaxError {
                                loc: Location::Point(self.input.len()),
                                message: "unexpected EOF in include directive".to_string(),
                            }))
                        }
                    };

                    while let Some((k, c)) = self.chars.next() {
                        if c == '>' {
                            match self.chars.next() {
                                Some((_, '\n')) => {
                                    return Some(Ok((i, Token::Include(&self.input[j..k]), k + 2)))
                                }
                                Some((_, d)) => {
                                    return Some(Err(SyntaxError {
                                        loc: Location::Point(k + 1),
                                        message: format!(
                                            "expected '\n' ending include directive, found {d:?}"
                                        ),
                                    }))
                                }
                                None => {
                                    return Some(Err(SyntaxError {
                                        loc: Location::Point(k + 1),
                                        message:
                                            "unexpected EOF in include directive: expected '\n'"
                                                .to_string(),
                                    }))
                                }
                            }
                        }
                    }

                    return Some(Err(SyntaxError {
                        loc: Location::Range(j, self.input.len()),
                        message: "unterminated include directive".to_string(),
                    }));
                }

                // Char literal.
                Some((i, '\'')) => {
                    let chr = match self.chars.next() {
                        Some((_, '\\')) =>
                        // escape sequence
                        {
                            match self.chars.next() {
                                Some((_, '\\')) => '\\',
                                Some((_, 't')) => '\t',
                                Some((_, 'n')) => '\n',
                                Some((_, '\'')) => '\'',
                                Some((j, c)) => {
                                    return Some(Err(SyntaxError {
                                        loc: Location::Point(j),
                                        message: format!("unknown escape sequence: {c:?}"),
                                    }))
                                }
                                None => {
                                    return Some(Err(SyntaxError {
                                        loc: Location::Point(self.input.len()),
                                        message: "unexpected EOF in char literal".to_string(),
                                    }))
                                }
                            }
                        }
                        Some((j, '\'')) => {
                            return Some(Err(SyntaxError {
                                loc: Location::Point(j),
                                message: "empty char literal".to_string(),
                            }))
                        }
                        Some((_, c)) if c.is_ascii_graphic() || c == ' ' => c,
                        Some((j, c)) => {
                            return Some(Err(SyntaxError {
                                loc: Location::Point(j),
                                message: format!("invalid character {c:?} in char literal"),
                            }))
                        }
                        None => {
                            return Some(Err(SyntaxError {
                                loc: Location::Range(i, self.input.len()),
                                message: "unexpected EOF in char literal".to_string(),
                            }))
                        }
                    };
                    match self.chars.next() {
                        Some((j, '\'')) => {
                            return Some(Ok((i, Token::CharLiteral(chr), j + 1)));
                        }
                        Some((j, c)) => {
                            return Some(Err(SyntaxError {
                                loc: Location::Point(j),
                                message: format!(
                                "unexpected character in char literal: expected '\'', found {c:?}"
                            ),
                            }))
                        }
                        None => {
                            return Some(Err(SyntaxError {
                                loc: Location::Point(self.input.len()),
                                message: "unexpected EOF in char literal: expected '\''"
                                    .to_string(),
                            }))
                        }
                    }
                }

                // Zero int literal.
                Some((i, '0')) => match self.chars.peek() {
                    Some((_, c)) if c.is_ascii_digit() => {
                        // Consume all leading zeros, so we get one error instead of multiple.
                        let mut j = i + 1;
                        while let Some((_, '0')) = self.chars.peek() {
                            j += 1;
                            self.chars.next();
                        }
                        return Some(Err(SyntaxError {
                            loc: Location::Range(i, j),
                            message: "invalid int literal: leading zeros".to_string(),
                        }));
                    }
                    _ => return Some(Ok((i, Token::IntLiteral(0), i + 1))),
                },

                // Nonzero int literal.
                Some((i, d)) if d.is_ascii_digit() => loop {
                    match self.chars.peek() {
                        Some((_, c)) if c.is_ascii_digit() => {
                            // Consume digit.
                            self.chars.next();
                        }
                        // Non-digit or EOF
                        p => {
                            let j = match p {
                                Some((j, _)) => *j,
                                None => self.input.len(),
                            };
                            match i64::from_str(&self.input[i..j]) {
                                Err(e) => match e.kind() {
                                    IntErrorKind::PosOverflow => return Some(Err(SyntaxError {
                                        loc: Location::Range(i, j),
                                        message:
                                            "int literal overflow, maximum 2^63, minimum -2^63+1"
                                                .to_string(),
                                    })),
                                    IntErrorKind::Empty => {
                                        panic!("lexer made an empty int literal")
                                    }
                                    IntErrorKind::InvalidDigit => {
                                        panic!("lexer made an int literal with an invalid digit")
                                    }
                                    _ => {
                                        panic!("Unknown error parsing int literal")
                                    }
                                },
                                Ok(n) => return Some(Ok((i, Token::IntLiteral(n), j))),
                            }
                        }
                    }
                },

                // Identifier or keyword.
                Some((i, c)) if c.is_ascii_alphabetic() || c == '_' => {
                    while let Some((_, c)) = self.chars.peek() {
                        if c.is_ascii_alphanumeric() || *c == '_' {
                            self.chars.next();
                        } else {
                            break;
                        }
                    }

                    let j = match self.chars.peek() {
                        Some((j, _)) => *j,
                        None => self.input.len(),
                    };

                    let tok = match &self.input[i..j] {
                        "bool" => Token::Bool,
                        "break" => Token::Break,
                        "continue" => Token::Continue,
                        "else" => Token::Else,
                        "false" => Token::False,
                        "for" => Token::For,
                        "if" => Token::If,
                        "int" => Token::Int,
                        "NULL" => Token::Null,
                        "return" => Token::Return,
                        "sizeof" => Token::SizeOf,
                        "true" => Token::True,
                        "void" => Token::Void,
                        "while" => Token::While,
                        s => Token::Identifier(s),
                    };

                    return Some(Ok((i, tok, j)));
                }

                // Structuring symbols.
                Some((i, '(')) => {
                    return Some(Ok((i, Token::LeftPar, i + 1)));
                }

                Some((i, ')')) => {
                    return Some(Ok((i, Token::RightPar, i + 1)));
                }

                Some((i, '{')) => {
                    return Some(Ok((i, Token::LeftBrace, i + 1)));
                }

                Some((i, '}')) => {
                    return Some(Ok((i, Token::RightBrace, i + 1)));
                }

                Some((i, '[')) => {
                    return Some(Ok((i, Token::LeftBracket, i + 1)));
                }

                Some((i, ']')) => {
                    return Some(Ok((i, Token::RightBracket, i + 1)));
                }

                Some((i, ';')) => {
                    return Some(Ok((i, Token::Semicolon, i + 1)));
                }

                Some((i, ',')) => {
                    return Some(Ok((i, Token::Comma, i + 1)));
                }

                // Operators.

                // '/' handled above as part of comment recognition.
                Some((i, '*')) => {
                    return Some(Ok((i, Token::Star, i + 1)));
                }

                Some((i, '%')) => {
                    return Some(Ok((i, Token::Percent, i + 1)));
                }

                Some((i, '=')) => {
                    if let Some((_, '=')) = self.chars.peek() {
                        self.chars.next();
                        return Some(Ok((i, Token::Equal, i + 2)));
                    } else {
                        return Some(Ok((i, Token::Assign, i + 1)));
                    }
                }

                Some((i, '!')) => {
                    if let Some((_, '=')) = self.chars.peek() {
                        self.chars.next();
                        return Some(Ok((i, Token::NotEqual, i + 2)));
                    } else {
                        return Some(Ok((i, Token::Exclamation, i + 1)));
                    }
                }

                Some((i, '&')) => {
                    if let Some((_, '&')) = self.chars.peek() {
                        self.chars.next();
                        return Some(Ok((i, Token::And, i + 2)));
                    } else {
                        return Some(Ok((i, Token::Ampersand, i + 1)));
                    }
                }

                Some((i, '|')) => {
                    if let Some((_, '|')) = self.chars.next() {
                        return Some(Ok((i, Token::Or, i + 1)));
                    } else {
                        return Some(Err(SyntaxError {
                            loc: Location::Point(i + 1),
                            message: "invalid operator: a single '|' is not allowed".to_string(),
                        }));
                    }
                }

                Some((i, '<')) => {
                    if let Some((_, '=')) = self.chars.peek() {
                        self.chars.next();
                        return Some(Ok((i, Token::Leq, i + 2)));
                    } else {
                        return Some(Ok((i, Token::Less, i + 1)));
                    }
                }

                Some((i, '>')) => {
                    if let Some((_, '=')) = self.chars.peek() {
                        self.chars.next();
                        return Some(Ok((i, Token::Geq, i + 2)));
                    } else {
                        return Some(Ok((i, Token::Greater, i + 1)));
                    }
                }

                Some((i, '+')) => {
                    if let Some((_, '+')) = self.chars.peek() {
                        self.chars.next();
                        return Some(Ok((i, Token::Increment, i + 2)));
                    } else {
                        return Some(Ok((i, Token::Plus, i + 1)));
                    }
                }

                Some((i, '-')) => {
                    if let Some((_, '-')) = self.chars.peek() {
                        self.chars.next();
                        return Some(Ok((i, Token::Decrement, i + 2)));
                    } else {
                        return Some(Ok((i, Token::Minus, i + 1)));
                    }
                }

                // Catchall
                Some((i, c)) => {
                    return Some(Err(SyntaxError {
                        loc: Location::Point(i),
                        message: format!("unknown character {c:?} encountered in input stream"),
                    }));
                }

                // EOF
                None => return None,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::lexer::{Lexer, Token};

    #[test]
    fn int() {
        let res: Vec<_> = Lexer::new("\t\n22 0 \n1234567890")
            .map(|r| r.unwrap().1)
            .collect();

        assert_eq!(
            res,
            &[
                Token::IntLiteral(22),
                Token::IntLiteral(0),
                Token::IntLiteral(1234567890),
            ]
        );

        let res: Vec<_> =
            Lexer::new(" 00040\t9999999999999999999999999999999999999999\n\n").collect();
        // Multiple leading zeros create one error.
        assert!(res[0].is_err());
        // The lexer can continue after consuming one leading zero.
        assert_eq!(res[1], Ok((4, Token::IntLiteral(40), 6)));
        // Too big to fit in 64 bits
        assert!(res[2].is_err());
    }

    #[test]
    fn line_comment() {
        let res: Vec<_> = Lexer::new(" 100// 🔠/ //yoo\n4/ /2")
            .map(|r| r.unwrap().1)
            .collect();
        // unwrap is called twice because next() returns Option<Result<_, _>>.
        assert_eq!(
            res,
            &[
                Token::IntLiteral(100),
                Token::IntLiteral(4),
                Token::Slash,
                Token::Slash,
                Token::IntLiteral(2),
            ]
        );
    }

    #[test]
    fn block_comment() {
        let mut lex = Lexer::new(
            // Block comments are stronger than Unicode tomfoolery, including emoji.
            // Surprisingly, Vim is able to actually correctly display the emoji on my machine,
            // in a nice font and color matching regular text. (Can Emacs do that for you?)
            "\n\t/*asœeɐɔɘɥɯɹʌʍʎif jkö 😂\\\\\\\\ßẞ̧̧̀̀̂́̃̋ȩ̀̈°̷́̃̋̂ˇ̇  // /* * /\r \n \x007 */",
        );
        assert_eq!(lex.next(), None);
        let res: Vec<_> = Lexer::new("*/*/*/*/").map(|r| r.unwrap().1).collect();
        assert_eq!(res, &[Token::Star, Token::Star, Token::Slash]);

        let mut lex = Lexer::new("101 /* \\n some comment text");
        assert_eq!(lex.next().unwrap().unwrap().1, Token::IntLiteral(101));
        assert!(lex.next().unwrap().is_err());
        assert!(lex.next().is_none());
    }

    #[test]
    fn identifier() {
        let mut lex = Lexer::new("_ a ARGH __hAxXoR_420__ 12_jklö asdf");
        assert_eq!(lex.next().unwrap().unwrap().1, Token::Identifier("_"));
        assert_eq!(lex.next().unwrap().unwrap().1, Token::Identifier("a"));
        assert_eq!(lex.next().unwrap().unwrap().1, Token::Identifier("ARGH"));
        assert_eq!(
            lex.next().unwrap().unwrap().1,
            Token::Identifier("__hAxXoR_420__")
        );
        assert_eq!(lex.next().unwrap().unwrap().1, Token::IntLiteral(12));
        assert_eq!(lex.next().unwrap().unwrap().1, Token::Identifier("_jkl"));
        assert!(lex.next().unwrap().is_err());
        assert_eq!(lex.next().unwrap().unwrap().1, Token::Identifier("asdf"));
        assert!(lex.next().is_none());
    }

    #[test]
    fn char() {
        let res: Vec<_> = Lexer::new(r#"'a' 'Z' '~' '/'' ''\\' '\n' '\t' '\''"#)
            .map(|r| r.unwrap().1)
            .collect();
        assert_eq!(
            res,
            &[
                Token::CharLiteral('a'),
                Token::CharLiteral('Z'),
                Token::CharLiteral('~'),
                Token::CharLiteral('/'),
                Token::CharLiteral(' '),
                Token::CharLiteral('\\'),
                Token::CharLiteral('\n'),
                Token::CharLiteral('\t'),
                Token::CharLiteral('\''),
            ]
        );

        for s in ["''", "'a", "'a ", "'ab", "'\r'", "'\\r'"] {
            let mut lex = Lexer::new(s);
            assert!(lex.next().unwrap().is_err());
        }
    }

    #[test]
    fn include() {
        let res: Vec<_> = Lexer::new("#include      < literally any character except >\n")
            .map(|r| r.unwrap().1)
            .collect();

        assert_eq!(res, &[Token::Include(" literally any character except ")]);

        let mut lex = Lexer::new(" asdf 2048 # #include\t #include<<>\n ");
        assert_eq!(lex.next(), Some(Ok((1, Token::Identifier("asdf"), 5))));
        assert_eq!(lex.next(), Some(Ok((6, Token::IntLiteral(2048), 10))));
        assert!(lex.next().unwrap().is_err());
        assert!(lex.next().unwrap().is_err());
        assert_eq!(lex.next(), Some(Ok((23, Token::Include("<"), 35))));
        assert!(lex.next().is_none());
    }

    #[test]
    fn keywords() {
        let res: Vec<_> = Lexer::new(
            "bool break continue else false for if int NULL return sizeof true void while",
        )
        .map(|r| r.unwrap().1)
        .collect();

        assert_eq!(
            res,
            &[
                Token::Bool,
                Token::Break,
                Token::Continue,
                Token::Else,
                Token::False,
                Token::For,
                Token::If,
                Token::Int,
                Token::Null,
                Token::Return,
                Token::SizeOf,
                Token::True,
                Token::Void,
                Token::While,
            ]
        );
        let res: Vec<_> = Lexer::new("boole bReak null include")
            .map(|r| r.unwrap().1)
            .collect();

        assert_eq!(
            res,
            &[
                Token::Identifier("boole"),
                Token::Identifier("bReak"),
                Token::Identifier("null"),
                Token::Identifier("include"),
            ]
        );
    }

    #[test]
    fn symbols() {
        // Not exhaustive, but we get a fair number.
        let res: Vec<_> = Lexer::new("=>=< =!=!&||&&;,][{}+--++-*/")
            .map(|r| r.unwrap().1)
            .collect();
        assert_eq!(
            res,
            &[
                Token::Assign,
                Token::Geq,
                Token::Less,
                Token::Assign,
                Token::NotEqual,
                Token::Exclamation,
                Token::Ampersand,
                Token::Or,
                Token::And,
                Token::Semicolon,
                Token::Comma,
                Token::RightBracket,
                Token::LeftBracket,
                Token::LeftBrace,
                Token::RightBrace,
                Token::Plus,
                Token::Decrement,
                Token::Increment,
                Token::Minus,
                Token::Star,
                Token::Slash,
            ]
        );

        let mut lex = Lexer::new("asdf|");
        lex.next();
        assert!(lex.next().unwrap().is_err());
        assert!(lex.next().is_none());
    }
}
