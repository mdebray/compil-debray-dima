use crate::lexer::{Lexer, Token};
use crate::types::StatementTag;
use crate::CompilerError;
use lalrpop_util::ParseError;

lalrpop_mod!(
    #[allow(clippy::just_underscores_and_digits)]
    #[allow(clippy::redundant_field_names)]
    #[allow(clippy::needless_lifetimes)]
    #[allow(clippy::too_many_arguments)]
    #[allow(clippy::extra_unused_lifetimes)]
    #[allow(clippy::clone_on_copy)]
    pub parser
);

pub fn parse(lex: Lexer) -> Result<SourceFile, SyntaxError> {
    parser::SourceFileParser::new()
        .parse(lex)
        .map_err(|e| e.into())
}

// Info on how to print error messages.
// Error messages must start with one line matching the following regex:
// ^[ \t]*File \("?\)\([^," \n\t<>]+\)\1, lines? \([0-9]+\)-?\([0-9]+\)?\(?:$\|,\
// \(?: characters? \([0-9]+\)-?\([0-9]+\)?:\)?\([ \n]Warning\(?: [0-9]+\)?:\)?\)
// (probably, found at
// https://github.com/emacs-mirror/emacs/blob/85108d541217f0333860c4f86c3b16b4349f85a4/lisp/progmodes/compile.el#L222
// )
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Location {
    Range(usize, usize),
    Point(usize),
}

impl Location {
    pub fn start(&self) -> usize {
        match self {
            Self::Range(i, _) | Self::Point(i) => *i,
        }
    }
}

#[derive(Debug, Eq, PartialEq, thiserror::Error)]
#[error("syntax error: {message}")]
pub struct SyntaxError {
    pub loc: Location,
    pub message: String,
}

impl CompilerError for SyntaxError {
    fn message(&self) -> String {
        format!("{self}")
    }

    fn location(&self) -> Location {
        self.loc
    }
}

impl<'a> From<ParseError<usize, Token<'a>, SyntaxError>> for SyntaxError {
    fn from(pe: ParseError<usize, Token<'a>, SyntaxError>) -> Self {
        match pe {
            ParseError::InvalidToken { location } => Self {
                loc: Location::Point(location),
                message: "invalid token".to_string(),
            },
            ParseError::UnrecognizedEOF { location, expected } => Self {
                loc: Location::Point(location),
                message: format!("unrecognized EOF, expected one of {}", expected.join(",")),
            },
            ParseError::UnrecognizedToken {
                token: (l, tok, r),
                expected,
            } => Self {
                loc: Location::Range(l, r),
                message: format!(
                    "unrecognized token \"{tok}\", expected one of {}",
                    expected.join(", ")
                ),
            },
            ParseError::ExtraToken { token: (l, tok, r) } => Self {
                loc: Location::Range(l, r),
                message: format!("extra token {tok}"),
            },
            ParseError::User { error } => error,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct SourceFile<'i> {
    pub includes: Vec<&'i str>,
    pub declarations: Vec<FunctionDeclaration<'i>>,
}

#[derive(Debug, Eq, PartialEq)]
pub struct FunctionDeclaration<'i> {
    pub loc: Location,
    pub return_type: Type,
    pub id: Identifier<'i>,
    pub params: Vec<(Type, Identifier<'i>, Location)>,
    pub body: Block<'i>,
}

pub type Block<'i> = Vec<DeclOrInstr<'i>>;

#[derive(Debug, Eq, PartialEq)]
pub enum DeclOrInstr<'i> {
    FunDecl(FunctionDeclaration<'i>),
    VarDecl(VarDeclaration<'i>),
    Instr(Statement<'i>),
}

#[derive(Debug, Eq, PartialEq)]
pub struct VarDeclaration<'i> {
    pub loc: Location,
    pub t: Type,
    pub id: Identifier<'i>,
    pub init: Option<LocExpression<'i>>,
}

pub type LocExpression<'i> = (Location, Expression<'i>);
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Expression<'i> {
    IntConstant(i64),
    CharConstant(char),
    BoolConstant(bool),
    NullConstant,
    Id(Identifier<'i>),
    FuncCall(Identifier<'i>, Vec<LocExpression<'i>>),
    UnaryOperation(UnaryOp, Box<LocExpression<'i>>),
    BinaryOperation(BinaryOp, Box<LocExpression<'i>>, Box<LocExpression<'i>>),
    SizeOf(Type),
}

#[derive(Debug, Eq, PartialEq)]
pub enum Statement<'i> {
    // ;
    Empty,
    Expr(LocExpression<'i>),
    IfElse {
        condition: LocExpression<'i>,
        s_if: Box<Statement<'i>>,
        s_else: Box<Statement<'i>>,
        tag: StatementTag,
    },
    For {
        condition: LocExpression<'i>,
        after: Vec<LocExpression<'i>>,
        body: Box<Statement<'i>>,
        tag: StatementTag,
    },
    Block(Block<'i>),
    Return {
        loc: Location,
        val: Option<LocExpression<'i>>,
    },
    Break(Location),
    Continue(Location),
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum BinaryOp {
    Assign,
    Equal,
    NotEqual,
    Less,
    Leq,
    Greater,
    Geq,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    And,
    Or,
}

impl std::fmt::Display for BinaryOp {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                BinaryOp::Assign => "=",
                BinaryOp::Equal => "==",
                BinaryOp::NotEqual => "!=",
                BinaryOp::Less => "<",
                BinaryOp::Leq => "<=",
                BinaryOp::Greater => ">",
                BinaryOp::Geq => ">=",
                BinaryOp::Add => "+",
                BinaryOp::Sub => "-",
                BinaryOp::Mul => "*",
                BinaryOp::Div => "/",
                BinaryOp::Mod => "%",
                BinaryOp::And => "&&",
                BinaryOp::Or => "||",
            }
        )
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum UnaryOp {
    Plus,
    Minus,
    Not,
    Reference,
    Deref,
    PreIncrement,
    PostIncrement,
    PreDecrement,
    PostDecrement,
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub struct Type(
    /// The underlying type
    pub BaseType,
    /// The number of stars in front of the base type
    pub usize,
);

impl std::fmt::Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)?;
        for _ in 0..self.1 {
            write!(f, "*")?;
        }
        Ok(())
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum BaseType {
    Void,
    Bool,
    Int,
}

impl std::fmt::Display for BaseType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                BaseType::Void => "void",
                BaseType::Bool => "bool",
                BaseType::Int => "int",
            }
        )
    }
}

pub type Identifier<'i> = &'i str;

#[cfg(test)]
mod tests {
    use crate::lexer::Lexer;
    use crate::syntax::*;

    #[test]
    fn empty() {
        let lexer = Lexer::new("");
        let parser = parser::SourceFileParser::new();
        assert_eq!(
            parser.parse(lexer),
            Ok(SourceFile {
                includes: vec![],
                declarations: vec![]
            })
        );
    }

    #[test]
    fn standard_include() {
        let lexer = Lexer::new(
            "#include <stdbool.h>
             #include <stdlib.h>
             #include <stdio.h>
             ",
        );
        let parser = parser::SourceFileParser::new();
        assert_eq!(
            parser.parse(lexer),
            Ok(SourceFile {
                includes: vec!["stdbool.h", "stdlib.h", "stdio.h"],
                declarations: vec![]
            })
        );
    }

    #[test]
    fn hello_world() {
        let lexer = Lexer::new(
            "int main() {
                putchar('H');
                putchar('e');
                putchar('l');
                putchar('l');
                putchar('o');
                putchar(' ');
                putchar('W');
                putchar('o');
                putchar('r');
                putchar('l');
                putchar('d');
                putchar('!');
                putchar('\\n');
                return 0;
            }
            ",
        );
        let res = parser::SourceFileParser::new().parse(lexer).unwrap();
        assert_eq!(res.declarations.len(), 1);
        assert!(res.includes.is_empty());
        let decl_instrs = &res.declarations[0].body;
        for (decl_instr, c) in decl_instrs.iter().zip("Hello World!\n".chars()) {
            match decl_instr {
                DeclOrInstr::Instr(Statement::Expr((_, Expression::FuncCall("putchar", v))))
                    if v.len() == 1 && v[0].1 == Expression::CharConstant(c) =>
                {
                    ()
                }
                _ => panic!(),
            }
        }

        match decl_instrs.last().unwrap() {
            DeclOrInstr::Instr(Statement::Return {
                loc: _,
                val: Some((_, Expression::IntConstant(0))),
            }) => (),
            _ => panic!(),
        }
    }

    #[test]
    fn swap() {
        let lexer = Lexer::new(
            "void**** main() {
                int a;
                a = b - a + (b = a);
                return &'a';
            }
            ",
        );

        let expected = SourceFile {
            includes: vec![],
            declarations: vec![FunctionDeclaration {
                loc: Location::Range(0, 15),
                return_type: Type(BaseType::Void, 4),
                id: "main",
                params: vec![],
                body: vec![
                    DeclOrInstr::VarDecl(VarDeclaration {
                        loc: Location::Range(34, 39),
                        t: Type(BaseType::Int, 0),
                        id: "a",
                        init: None,
                    }),
                    DeclOrInstr::Instr(Statement::Expr((
                        Location::Range(57, 76),
                        Expression::BinaryOperation(
                            BinaryOp::Assign,
                            Box::new((Location::Range(57, 58), Expression::Id("a"))),
                            Box::new((
                                Location::Range(61, 76),
                                Expression::BinaryOperation(
                                    BinaryOp::Add,
                                    Box::new((
                                        Location::Range(61, 66),
                                        Expression::BinaryOperation(
                                            BinaryOp::Sub,
                                            Box::new((
                                                Location::Range(61, 62),
                                                Expression::Id("b"),
                                            )),
                                            Box::new((
                                                Location::Range(65, 66),
                                                Expression::Id("a"),
                                            )),
                                        ),
                                    )),
                                    Box::new((
                                        Location::Range(70, 75),
                                        Expression::BinaryOperation(
                                            BinaryOp::Assign,
                                            Box::new((
                                                Location::Range(70, 71),
                                                Expression::Id("b"),
                                            )),
                                            Box::new((
                                                Location::Range(74, 75),
                                                Expression::Id("a"),
                                            )),
                                        ),
                                    )),
                                ),
                            )),
                        ),
                    ))),
                    DeclOrInstr::Instr(Statement::Return {
                        loc: Location::Range(94, 106),
                        val: Some((
                            Location::Range(101, 105),
                            Expression::UnaryOperation(
                                UnaryOp::Reference,
                                Box::new((
                                    Location::Range(102, 105),
                                    Expression::CharConstant('a'),
                                )),
                            ),
                        )),
                    }),
                ],
            }],
        };

        assert_eq!(Ok(expected), parser::SourceFileParser::new().parse(lexer));
    }

    #[test]
    fn increment_constant() {
        let lexer = Lexer::new("void main() { return 1++; }");

        let expected = SourceFile {
            includes: vec![],
            declarations: vec![FunctionDeclaration {
                loc: Location::Range(0, 11),
                return_type: Type(BaseType::Void, 0),
                id: "main",
                params: vec![],
                body: vec![DeclOrInstr::Instr(Statement::Return {
                    loc: Location::Range(14, 25),
                    val: Some((
                        Location::Range(21, 24),
                        Expression::UnaryOperation(
                            UnaryOp::PostIncrement,
                            Box::new((Location::Range(21, 22), Expression::IntConstant(1))),
                        ),
                    )),
                })],
            }],
        };

        assert_eq!(Ok(expected), parser::SourceFileParser::new().parse(lexer));
    }
}
