use clap::{self, Parser};
use std::io::Read;

#[macro_use]
extern crate lalrpop_util;

mod codegen;
mod lexer;
mod syntax;
mod types;

#[derive(clap::Parser)]
#[command(name = "petitc")]
#[command(author = "Maurice Debray, Simon Dima")]
#[command(version, about, long_about)]
#[command(group(
            clap::ArgGroup::new("mode")
                .args(["parse_only", "type_only"]),
        ))]
struct Cli {
    file: std::path::PathBuf,

    #[arg(long)]
    parse_only: bool,
    #[arg(long)]
    type_only: bool,
}

#[derive(thiserror::Error, Debug)]
#[error("{0}")]
struct BeautifulError(String);

trait CompilerError {
    fn location(&self) -> syntax::Location;
    fn message(&self) -> String;
    fn pretty_print(&self, filename: &str, l: &Locator) -> BeautifulError {
        let location_string = match l.locate(self.location()) {
            XYLocation::Point(l, c) => {
                format!("line {l}, character {c}")
            }
            XYLocation::Range(l1, c1, l2, c2) => {
                format!("lines {l1}-{l2}, characters {c1}-{c2}")
            }
        };
        BeautifulError(format!(
            "File \"{filename}\", {location_string}:\n{0}\n",
            self.message()
        ))
    }
}

struct Locator(Vec<(usize, usize)>);

impl Locator {
    fn locate(&self, loc: syntax::Location) -> XYLocation {
        match loc {
            syntax::Location::Point(i) => {
                let (l, c) = self.0[i];
                XYLocation::Point(l, c)
            }
            syntax::Location::Range(i, j) => {
                let (l1, c1) = self.0[i];
                let (l2, c2) = self.0[j];
                XYLocation::Range(l1, c1, l2, c2)
            }
        }
    }

    fn new(source: &str) -> Self {
        let mut ret = Vec::new();
        let mut line = 1;
        let mut col = 1;
        for c in source.chars() {
            ret.push((line, col));
            if c == '\n' {
                col = 1;
                line += 1;
            } else {
                col += 1;
            }
        }
        ret.push((line, col));
        Self(ret)
    }
}

enum XYLocation {
    Point(usize, usize),
    Range(usize, usize, usize, usize),
}

#[derive(Debug, thiserror::Error)]
enum Error {
    #[error("IO errorː {0}")]
    IoError(#[from] std::io::Error),
    #[error("{0}")]
    BeautifulError(#[from] BeautifulError),
}
impl Error {
    fn is_internal(&self) -> bool {
        match self {
            Self::IoError(_) => true,
            Self::BeautifulError(_) => false,
        }
    }
}

fn main() {
    if let Err(e) = notmain() {
        eprintln!("{e}");
        if e.is_internal() {
            std::process::exit(2);
        } else {
            std::process::exit(1);
        }
    }
}

fn notmain() -> Result<(), Error> {
    let cli = Cli::parse();

    let mut f = std::fs::File::open(&cli.file)?;
    let filename = cli.file.clone().into_os_string().into_string().unwrap();

    let mut source = String::new();
    f.read_to_string(&mut source)?;

    let locator = Locator::new(&source);

    let lexer = lexer::Lexer::new(&source);
    let parsed = syntax::parse(lexer).map_err(|x| x.pretty_print(&filename, &locator))?;

    if cli.parse_only {
        return Ok(());
    }

    let typed = types::type_file(&parsed).map_err(|x| x.pretty_print(&filename, &locator))?;

    if cli.type_only {
        return Ok(());
    }

    let mut asm_name = filename.clone();
    asm_name.truncate(filename.len() - 2);
    asm_name.push_str(".s");

    let code = codegen::gen_code(&typed);
    let mut asm_file = cli.file;
    asm_file.set_extension("s");
    code.print_in(&asm_file.into_os_string().into_string().unwrap())?;
    Ok(())
}
