use crate::types::{
    CrementOp, CrementTime, CrementType, StatementTag, TBinaryOp, TExpression, TFun, TStatement,
    TUnaryOp,
};
use write_x86_64::file::File;
use write_x86_64::reg::{Label, Operand, RegB, RegQ};
use write_x86_64::{
    addq, addr, andq, call, cmpq, cqto, idivq, immq, imulq, instr::Cond, jcc, jmp, leaq, movq,
    movsbq, negq, popq, pushq, reg, ret, set, shlq, shrq, subq, Data, Text,
};

fn for_exit_label(l: &StatementTag) -> Label {
    Label::from_str(format!("for_exit_{}", l))
}

fn for_incr_label(l: &StatementTag) -> Label {
    Label::from_str(format!("for_incr_{}", l))
}

fn endif_label(l: &StatementTag) -> Label {
    Label::from_str(format!("endif_{}", l))
}

fn else_label(l: &StatementTag) -> Label {
    Label::from_str(format!("else_{}", l))
}

fn lazy_label(l: &StatementTag) -> Label {
    Label::from_str(format!("lazy_{}", l))
}

fn for_loop_label(l: &StatementTag) -> Label {
    Label::from_str(format!("for_loop_{}", l))
}

pub fn gen_code(fun_list: &Vec<TFun>) -> File {
    let mut text = Text::empty();
    for f in fun_list {
        func_code(f, &mut text);
    }
    malloc_and_putchar(&mut text);
    File {
        text_ss: text,
        data_ss: Data::empty(),
        globl: Some(Label::from_str("main".to_string())),
    }
}

fn malloc_and_putchar(text: &mut Text) {
    *text += Text::label(Label::from_str("_custom_malloc".to_string()));
    *text += pushq(reg!(RegQ::Rbp));
    *text += movq(reg!(RegQ::Rsp), reg!(RegQ::Rbp));
    *text += movq(addr!(24, RegQ::Rsp), reg!(RegQ::Rdi));
    *text += andq(immq(-16), reg!(RegQ::Rsp));
    *text += call(Label::from_str("malloc".to_string()));
    return_asm(text);
    *text += Text::label(Label::from_str("_custom_putchar".to_string()));
    *text += pushq(reg!(RegQ::Rbp));
    *text += movq(reg!(RegQ::Rsp), reg!(RegQ::Rbp));
    *text += movq(addr!(24, RegQ::Rsp), reg!(RegQ::Rdi));
    // -16 is 1111_1111_1111_0000 in 2's complement
    *text += movq(reg!(RegQ::Rsp), reg!(RegQ::Rbp));
    *text += andq(immq(-16), reg!(RegQ::Rsp));
    *text += call(Label::from_str("putchar".to_string()));
    return_asm(text);
}

fn func_code(f: &TFun, text: &mut Text) {
    let TFun(fun_label, offset, statments) = f;
    *text += Text::label(Label::from_str(fun_label.clone()));
    *text += pushq(reg!(RegQ::Rbp));
    *text += movq(reg!(RegQ::Rsp), reg!(RegQ::Rbp));
    *text += addq(Operand::Imm(*offset), reg!(RegQ::Rsp));

    block_code(statments, text);

    return_asm(text);
}

fn block_code(statements: &Vec<TStatement>, text: &mut Text) {
    for s in statements {
        statement_code(s, text);
    }
}

fn statement_code(s: &TStatement, text: &mut Text) {
    match s {
        TStatement::Expr(e) => {
            expr_code(e, text);
            *text += addq(Operand::Imm(8), reg!(RegQ::Rsp));
        }
        TStatement::Return(e) => {
            expr_code(e, text);
            *text += popq(RegQ::Rax);
            return_asm(text);
        }
        TStatement::ReturnNothing => return_asm(text),
        TStatement::IfElse(e, yes, no, tag) => {
            expr_code(e, text);
            let else_statment = else_label(tag);
            let endif = endif_label(tag);
            *text += popq(RegQ::R8);
            *text += addq(immq(0), reg!(RegQ::R8));
            *text += jcc(Cond::E, else_statment.clone());
            statement_code(yes, text);
            *text += jmp(endif.clone());
            *text += Text::label(else_statment);
            statement_code(no, text);
            *text += Text::label(endif);
        }
        TStatement::Break(l) => {
            *text += jmp(for_exit_label(l));
        }
        TStatement::Continue(l) => {
            *text += jmp(for_incr_label(l));
        }
        TStatement::For(cond, exprs, body, tag) => {
            *text += Text::label(for_loop_label(tag));
            expr_code(cond, text);
            *text += popq(RegQ::R8);
            *text += addq(immq(0), reg!(RegQ::R8));
            *text += jcc(Cond::E, for_exit_label(tag));
            statement_code(body, text);
            *text += Text::label(for_incr_label(tag));
            for e in exprs {
                expr_code(e, text);
            }
            *text += jmp(for_loop_label(tag));
            *text += Text::label(for_exit_label(tag));
        }
        TStatement::Block(block) => block_code(block, text),
        TStatement::Empty => (),
    }
}

fn expr_code(e: &TExpression, text: &mut Text) {
    match e {
        TExpression::IntConstant(i) => {
            *text += pushq(immq(*i));
        }
        TExpression::UnaryOperation(unop, exp) => unop_code(unop, exp, text),
        TExpression::BinaryOperation(binop, lexp, rexp) => binop_code(binop, lexp, rexp, text),
        TExpression::VarAddress(offset, indir_no) => {
            *text += movq(reg!(RegQ::Rbp), reg!(RegQ::R8));
            for _ in 0..*indir_no {
                *text += movq(addr!(16, RegQ::R8), reg!(RegQ::R8));
            }
            *text += addq(immq(*offset), reg!(RegQ::R8));
            *text += pushq(reg!(RegQ::R8));
        }
        TExpression::IntToBoolCast(exp) => {
            expr_code(exp, text);
            *text += popq(RegQ::R8);
            *text += addq(immq(0), reg!(RegQ::R8));
            *text += set(Cond::NE, reg!(RegB::R9b));
            *text += movsbq(reg!(RegB::R9b), RegQ::R9);
            *text += pushq(reg!(RegQ::R9));
        }
        TExpression::FuncCall(fun_label, args, indir_no) => {
            //push arguments on stack
            for e in args.iter().rev() {
                expr_code(e, text);
            }
            //Add rbp of static parent
            *text += movq(reg!(RegQ::Rbp), reg!(RegQ::R8));
            for _ in 0..*indir_no {
                *text += movq(addr!(16, RegQ::R8), reg!(RegQ::R8));
            }
            *text += pushq(reg!(RegQ::R8));
            // call !
            *text += call(Label::from_str(fun_label.clone()));
            // pop parent stack frame
            *text += popq(RegQ::R10);
            // pop arguments
            for _ in args {
                *text += popq(RegQ::R10);
            }
            *text += pushq(reg!(RegQ::Rax));
        }
        TExpression::Pointerfy(exp) => {
            expr_code(exp, text);
            *text += shlq(immq(3), addr!(RegQ::Rsp));
        }
        TExpression::DePointerfy(exp) => {
            expr_code(exp, text);
            *text += shrq(immq(3), addr!(RegQ::Rsp));
        }
        TExpression::LazyOr(lexp, rexp, tag) => {
            expr_code(lexp, text);
            *text += popq(RegQ::R8);
            *text += addq(immq(0), reg!(RegQ::R8));
            *text += jcc(Cond::NE, lazy_label(tag));
            expr_code(rexp, text);
            *text += popq(RegQ::R8);
            *text += Text::label(lazy_label(tag));
            *text += pushq(reg!(RegQ::R8));
        }
        TExpression::LazyAnd(lexp, rexp, tag) => {
            expr_code(lexp, text);
            *text += popq(RegQ::R8);
            *text += addq(immq(0), reg!(RegQ::R8));
            *text += jcc(Cond::E, lazy_label(tag));
            expr_code(rexp, text);
            *text += popq(RegQ::R8);
            *text += Text::label(lazy_label(tag));
            *text += pushq(reg!(RegQ::R8));
        }
    }
}

fn binop_code(binop: &TBinaryOp, lexp: &TExpression, rexp: &TExpression, text: &mut Text) {
    expr_code(lexp, text);
    expr_code(rexp, text);
    match binop {
        TBinaryOp::Assign => {
            *text += popq(RegQ::R8);
            *text += popq(RegQ::R9);
            *text += movq(reg!(RegQ::R8), addr!(RegQ::R9));
            *text += pushq(reg!(RegQ::R8));
        }
        TBinaryOp::Leq => {
            *text += popq(RegQ::R8);
            *text += cmpq(addr!(0, RegQ::Rsp), reg!(RegQ::R8));
            *text += popq(RegQ::R8);
            *text += pushq(immq(0));
            *text += set(Cond::GE, addr!(RegQ::Rsp));
        }
        TBinaryOp::NotEqual => {
            *text += popq(RegQ::R8);
            *text += cmpq(addr!(0, RegQ::Rsp), reg!(RegQ::R8));
            *text += popq(RegQ::R8);
            *text += pushq(immq(0));
            *text += set(Cond::NE, addr!(RegQ::Rsp));
        }
        TBinaryOp::Less => {
            *text += popq(RegQ::R8);
            *text += cmpq(addr!(0, RegQ::Rsp), reg!(RegQ::R8));
            *text += popq(RegQ::R8);
            *text += pushq(immq(0));
            *text += set(Cond::G, addr!(RegQ::Rsp));
        }
        TBinaryOp::Equal => {
            *text += popq(RegQ::R8);
            *text += cmpq(addr!(0, RegQ::Rsp), reg!(RegQ::R8));
            *text += popq(RegQ::R8);
            *text += pushq(immq(0));
            *text += set(Cond::E, addr!(RegQ::Rsp));
        }
        TBinaryOp::Add => {
            *text += popq(RegQ::R8);
            *text += addq(reg!(RegQ::R8), addr!(0, RegQ::Rsp));
        }
        TBinaryOp::Sub => {
            *text += popq(RegQ::R8);
            *text += subq(reg!(RegQ::R8), addr!(0, RegQ::Rsp));
        }

        TBinaryOp::Mul => {
            *text += popq(RegQ::R8);
            *text += imulq(addr!(0, RegQ::Rsp), reg!(RegQ::R8));
            *text += movq(reg!(RegQ::R8), addr!(RegQ::Rsp));
        }

        TBinaryOp::Div | TBinaryOp::Mod => {
            *text += popq(RegQ::R8);
            *text += popq(RegQ::Rax);
            *text += cqto();
            *text += idivq(reg!(RegQ::R8));
            match binop {
                TBinaryOp::Mod => *text += pushq(reg!(RegQ::Rdx)),
                TBinaryOp::Div => *text += pushq(reg!(RegQ::Rax)),
                _ => unreachable!(),
            }
        }
    }
}

fn unop_code(unop: &TUnaryOp, exp: &TExpression, text: &mut Text) {
    expr_code(exp, text);
    match unop {
        TUnaryOp::Minus => {
            *text += negq(addr!(RegQ::Rsp));
        }
        TUnaryOp::Not => {
            // TODO make it better (see decrementation of bools)
            *text += popq(RegQ::R8);
            *text += addq(immq(0), reg!(RegQ::R8));
            *text += set(Cond::E, reg!(RegB::R9b));
            *text += movsbq(reg!(RegB::R9b), RegQ::R9);
            *text += pushq(reg!(RegQ::R9));
        }
        TUnaryOp::Deref => {
            *text += popq(RegQ::R8);
            *text += pushq(addr!(RegQ::R8));
        }
        TUnaryOp::Crement(op, time, typ) => {
            *text += popq(RegQ::R8);
            match time {
                CrementTime::Pre => {
                    crement(op, typ, text);
                    *text += pushq(addr!(RegQ::R8));
                }
                CrementTime::Post => {
                    *text += pushq(addr!(RegQ::R8));
                    crement(op, typ, text);
                }
            }
        }
    }
}

fn crement(op: &CrementOp, typ: &CrementType, text: &mut Text) {
    let crement_value = match op {
        CrementOp::Increment => 1,
        CrementOp::Decrement => -1,
    };
    match typ {
        CrementType::Int => *text += addq(immq(crement_value), addr!(RegQ::R8)),
        CrementType::Bool => {
            if crement_value == 1 {
                *text += leaq(immq(1), RegQ::R9);
                *text += movq(reg!(RegQ::R9), addr!(RegQ::R8));
            } else {
                *text += addq(immq(0), addr!(RegQ::R8));
                *text += movq(immq(0), addr!(RegQ::R8));
                *text += set(Cond::E, addr!(RegQ::R8));
            }
        }
        CrementType::Pointer => *text += addq(immq(8 * crement_value), addr!(RegQ::R8)),
    }
}

fn return_asm(text: &mut Text) {
    //TODO: use leave but not sure of its behavior
    *text += movq(reg!(RegQ::Rbp), reg!(RegQ::Rsp));
    *text += popq(RegQ::Rbp);
    *text += ret();
}
