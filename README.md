# Compilateur de `petitc` écrit en rust

Compilateur de `petitc` écrit en Rust par Maurice Debray et Simon Dima pour le projet du cours de compilation.

## Compilation

### Dépendances

Pour compiler ce projet, une toolchain récente de Rust est nécessaire, avec au moins Cargo et rustc.

Les crates Rust dont dépend le compilateur, spécifiées dans le fichier `Cargo.toml`, seront automatiquement installées par Cargo.

### Compiler le projet

Le makefile fourni permet de compiler le compilateur. La commande `make` compile le compilateur et crée un symlink à la racine du projet qui pointe vers le binaire compilé.

Une dérivation nix est aussi fournie.

## Utilisation

Documentation de la ligne de commande:
```
Usage: petitc [OPTIONS] <FILE>

Arguments:
  <FILE>

Options:
      --parse-only
      --type-only
  -h, --help        Print help information
  -V, --version     Print version information
```


### Executer les tests

`cargo test` execute les tests fournis par Jean-Christophe Filliatre dans l'énoncé du projet et quelques tests additionnels.

## Notes sur le projet

### Lexing

Le lexer est écrit à la main. Il s'agit globalement d'un automate s'executant sur les mots du code source.

### Parsing

Nous avons utilisé le générateur de parser [lalrpop](https://github.com/lalrpop/lalrpop), qui est meilleur que sa documentation
(en particulier, la syntaxe pour utiliser un lexer customisé est assez étrange et mal documentée).

Pour éviter le problème du [dangling else](https://en.wikipedia.org/wiki/Dangling_else),
on distingue les statements selons s'ils finissent ou non par un `if`, pour interdire le parsage `if {if {}} else {}`
et autoriser comme C uniquement `if {if {} else {}}`.
Ceci nécessite de dupliquer certaines règles.

Pour le parsage des expressions, nous introduisons plusieurs niveaux de précédence, la plupart générés avec une macro lalrpop.
Quelques légères ambigüités dans le sujet ont été résolues en nous tenant aux [règles de C](https://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#Operator_precedence).

Tous les identifiants renvoyés par le parser sont des `&str` et non pas des `String`,
et ne sont donc pas des copies des identifiants présents dans le code source mais des références.
Ceci permet d'éviter de copier des chaînes de caractères, ce qui est une optimisation entièrement inutile
à l'échelle des petits programmes que nous compilons, mais constitue un bon exercice de Rust.
La lifetime de ces références est notée `'input` dans le code du parser et `'i` ailleurs.

Le parser renvoie aussi les positions dans le code source des éléments syntactiques, pour permettre de localiser précisément les erreurs.

### Typage

Le typage (en plus de vérifier la correction sémantique du code) effectue trois opérations notables facilitant la compilation (ie rendant la sémantique plus claire):

  - **`Pointerfy`, `DePointerfy`:** Nous avons introduit des noeuds représentant les conversions d'entiers en pointeurs lors d'une somme de type `x* + int` et et de pointeurs en entiers lors d'une différence de pointeurs (`x* - x*`).
Cela se traduit lors de la génération de code par des shifts vers la droite ou la gauche de 4 bits.
  - **Accès aux variables:** Chaque occurence d'un identifiant de variable est converti au typage en une expression `VarAddress`,
qui permet de placer sur la pile un pointeur vers la position en mémoire de la variable (en remontant vers le tableau d'activation
approprié dans le cas d'une variable provenant d'un parent d'une fonction imbriquée),
et une expression `Deref`, qui est la même que le déréférencement d'un pointeur.
Autrement dit, cela correspond à remplacer `x` par `*&x`.
Ceci permet de traiter exactement pareil le déréférencement d'un pointeur et l'accès à une variable,
ainsi que l'assignement à un pointeur et à une variable.
  - **Fonctions imbriquées:** Lors du typage, les déclarations de fonctions sont évaluées puis poussées dans un tableau contenant toutes les fonctions (imbriquées ou non).
Cela est rendu possible par le fait qu'on mémorise lors d'un appel de fonction le nombre de tableaux d'activation à remonter pour trouver le tableaux d'activation du parent statique.
Ainsi cet aplatissement de l'arbre ne fait pas perdre d'information.

C'est au typage que les noms de fonctions sont décorés avec leur position dans le code source, afin de donner un nom réellement unique
aux fonctions différentes.
Les boucles sont aussi associées à un tag qui permettra de créer des étiquettes uniques dans le code assembleur pour les instructions
`break` et `continue`.

Le plus grand obstacle pour cette partie a été la tentative de Simon de réaliser en Rust une structure de données récursive
pour représenter les environnements (qui est présente dans le code rendu pour le typage).
La décision de remplacer celle-ci par, essentiellement, un `&mut Vec`, a permis de grandement simplifier le code.

### Génération de code

Le typage renvoie un AST qui rend la génération de code assez simple.
Il suffit alors de filtrer l'arbre et de générer l'assembleur.
C'est assez direct grace à la crate [write_x86_64](https://docs.rs/write_x86_64/latest/write_x86_64/index.html)

Un `return 0` est rajouté à la fin de chaque fonction.

### Interface terminal
La crate [clap](https://docs.rs/clap/latest/clap/) est utilisée pour la lecture des arguments en ligne de commande.
